package ryze.workflow;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

public class PropertiesWorkflowTest {

	
	@Before
	public void setup() throws IOException{
		System.setProperty(PropertiesWorkflowLoader.PROP_NAME, "workflows");
	}

	@Test
	public void testSimpleWorkflow() throws Exception {

		@SuppressWarnings("rawtypes")
		List<WorkflowInfo> wfs = PropertiesWorkflowLoader.loadWorkflows();
		wfs.forEach((k) -> System.out.println(k));
	}

}
