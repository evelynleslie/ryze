package ryze.analytics;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class BuggyAnalyzerTest {

	
	@Test
	public void buggyanalyzer_nolines_all0inbuggyanalysis(){
		List<String> noLines = new  LinkedList<>(); //here is an example on how to create input corresponding to no input lines
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(noLines).get(0);
		assertEquals(new BuggyAnalysis(0,0,0,0,0.0), lysis);
	}
	
	@Test
	public void buggyanalyzer_threeLines_totalLinesIs3(){
		List<String> threeLines = Arrays.asList(new String[]{"hello", "beautiful", "world"}); // an example of short input
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(threeLines).get(0);
		assertEquals(3, lysis.getTotalLines());
	}

	@Test
	public void buggyanalyzer_multLines_totalLinesIs7(){ 
		List<String> multLines = Arrays.asList(new String[]{ // an example of longer input
				"it",
				"was",
				"the",
				"best",
				"of",
				"times, it was the worst",
				"of times",
		});

		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(multLines).get(0);
		assertEquals(7, lysis.getTotalLines());
	}

	@Test
	public void buggyanalyzer_wordDelimitation_TotalLinesIs1(){ 
		List<String> wordDelimitation = Arrays.asList(new String[]{
				"book mark"
		});

		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(wordDelimitation).get(0); 
		assertEquals(1, lysis.getTotalLines());
	}

	@Test
	public void buggyanalyzer_wordCount_TotalWordsIs4(){ 
		List<String> wordCount = Arrays.asList(new String[]{
				"book, mark",
				"marking criteria"
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(wordCount).get(0); 
		assertEquals(4, lysis.getTotalWords());
	}

	@Test
	public void buggyanalyzer_totalWordCount_TotalWordsIs69(){
		List<String> totalWordCount = Arrays.asList(new String[]{
				"Lorem ipsum dolor sit amet, "
				+ "consectetur adipiscing elit, "
				+ "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "
				+ "Ut enim ad minim veniam, "
				+ "quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "
				+ "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. "
				+ "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(totalWordCount).get(0); 
		assertEquals(69, lysis.getTotalWords());
	}

	@Test
	public void buggyanalyzer_avgLineLength_resultExpectedIs2point5(){ 
		List<String> avgLineLen = Arrays.asList(new String[]{
				"book, mark",
				"marking criteria for"
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(avgLineLen).get(0); 
		assertEquals(2.5, lysis.getAvgLineLength(), 0.01);
		
	}
	@Test
	public void buggyanalyzer_minLineLength_resultExpectedIs2(){ 
		List<String> minLineLen= Arrays.asList(new String[]{
				"book, mark",
				"marking criteria for stuff"	
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(minLineLen).get(0); 
		assertEquals(2, lysis.getMinLineLength());
	}
	@Test
	public void buggyanalyzer_maxLineLength_resultExpectedIs4(){ 
		List<String> maxLineLen= Arrays.asList(new String[]{
				"book, mark",
				"marking criteria for stuff"
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(maxLineLen).get(0); 
		assertEquals(4, lysis.getMaxLineLength());
	}
}
