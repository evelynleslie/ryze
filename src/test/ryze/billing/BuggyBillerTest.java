package ryze.billing;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import ryze.analytics.BuggyAnalysis;

public class BuggyBillerTest {

	@Test
	public void buggybiller_allZeroAnalysis_0() {
		BuggyBiller bb = new BuggyBiller(3, 7);
		BuggyAnalysis lysis = new BuggyAnalysis(0, 0, 0, 0, 0.0);
		assertEquals(0.0, bb.bill(Arrays.asList(lysis)).get(0), 0.01);
	}

	@Test
	public void buggybiller_lowerRate_resultExpectedIs30() {
		BuggyBiller bb = new BuggyBiller(3, 7);
		BuggyAnalysis lysis = new BuggyAnalysis(10, 55, 1, 10, 5.5);

		assertEquals(30.0, bb.bill(Arrays.asList(lysis)).get(0), 0.01);
	}

	@Test
	public void buggybiller_higherRate_resultExpectedIs70() {
		BuggyBiller bb = new BuggyBiller(3, 7);
		BuggyAnalysis lysis = new BuggyAnalysis(10, 58, 1, 10, 5.8);
		assertEquals(70.0, bb.bill(Arrays.asList(lysis)).get(0), 0.01);
	}
}
