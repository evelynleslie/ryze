package ryze.analytics;

import java.util.LinkedList;
import java.util.List;

public class BuggyAnalyzer implements Analyzer<String, BuggyAnalysis> {

	@Override
	public List<BuggyAnalysis> analyze(List<String> lines) {
		List<BuggyAnalysis> res = new LinkedList<>();

		BuggyAnalysis lysis = new BuggyAnalysis();
		res.add(lysis);

		lysis.setTotalLines(calcTotalLines(lines));
		lysis.setTotalWords(calcTotalWords(lines));
		lysis.setMinLineLength(calcMinLineLength(lines));
		lysis.setMaxLineLength(calcMaxLineLength(lines));
		lysis.setAvgLineLength(calcAvgLineLength(lines));
		return res;
	}

	private double calcAvgLineLength(List<String> lines) {
		// checking if there is a line
		// to avoid division by 0 error
		if (lines.size() == 0) {
			return 0;
		}
		// conversion of calcTotalWords and calcTotalLines into double from int
		// in order to use double division
		return (double) calcTotalWords(lines) / (double) calcTotalLines(lines);
	}

	private int calcMaxLineLength(List<String> lines) {
		int maxLineLen = 0;
		// for loop changed to start from 0 instead of 1
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			// line is split for every non alphanumeric character found
			String[] words = line.split("[^\\w']+");
			int lineLen = 0;
			// for loop changed to start from 0 instead of 1
			for (int j = 0; j < words.length; j++) {
				lineLen++;
			}
			// change maxLineLen to the first iteration value of LineLen
			// to avoid cases where no lineLen is bigger
			if (i == 0) {
				maxLineLen = lineLen;
			}
			// if lineLen is bigger than maxLineLen instead of the other way
			// around
			if (lineLen > maxLineLen)
				maxLineLen = lineLen;
		}
		return maxLineLen;
	}

	private int calcMinLineLength(List<String> lines) {
		int minLineLen = 0;
		// for loop changed to start from 0 instead of 1
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			// line is split for every non alphanumeric character found
			String[] words = line.split("[^\\w']+");
			int lineLen = 0;
			// for loop changed to start from 0 instead of 1
			for (int j = 0; j < words.length; j++) {
				lineLen++;
			}
			// change maxLineLen to the first iteration value of LineLen
			// to avoid cases where no lineLen is smaller
			if (i == 0) {
				minLineLen = lineLen;
			}
			// if lineLen is smaller than maxLineLen instead of the other way
			// around
			if (lineLen < minLineLen)
				minLineLen = lineLen;
		}
		return minLineLen;
	}

	private int calcTotalWords(List<String> lines) {
		int wordcount = 0;
		// for loop changed to start from 0 instead of 1
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			// line is split for every non alphanumeric character found
			String[] words = line.split("[^\\w']+");
			for (int j = 0; j < words.length; j++) {
				wordcount++;
			}
		}
		return wordcount;
	}

	private int calcTotalLines(List<String> lines) {
		int count = 0;
		// for loop changed to start from 0 instead of 1
		for (int i = 0; i < lines.size(); i++) {
			count++;

		}
		return count;
	}

}
