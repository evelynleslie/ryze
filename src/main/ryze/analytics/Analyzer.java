package ryze.analytics;

import java.util.List;

public interface Analyzer<A, B> {

	public List<B> analyze (List<A> tweets);
}
