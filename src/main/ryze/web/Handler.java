package ryze.web;

import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;

public interface Handler {

	public Response handle(Method m, String uri, IHTTPSession session);
}
