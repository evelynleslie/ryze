package ryze.web;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.rythmengine.Rythm;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;
import fi.iki.elonen.NanoHTTPD.ResponseException;
import ryze.workflow.WorkflowInfo;

public class WorkflowsHandler implements Handler {

	private WorkflowService wfSvc;
	
	public WorkflowsHandler(){
		wfSvc = WorkflowService.getInstance();
	}

	@Override
	public Response handle(Method m, String uri, IHTTPSession session) {
		Map<String, String> files = new HashMap<>();
		try {
			session.parseBody(files);
		} catch (IOException ioe) {
			return NanoHTTPD.newFixedLengthResponse(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "SERVER INTERNAL ERROR: IOException: " + ioe.getMessage());
        } catch (ResponseException re) {
            return NanoHTTPD.newFixedLengthResponse(re.getStatus(), NanoHTTPD.MIME_PLAINTEXT, re.getMessage());
        }
		
		
		Map<String, String> params = session.getParms();
		String reload = params.get("action");
		
		try {
			List<WorkflowInfo> wfs = wfSvc.getWorkflows(reload != null);
			String loadTime = wfSvc.getLoadTime().format(DateTimeFormatter.ofPattern("MM-dd HH:mm"));
			String msg = Rythm.render("list-workflows.html", wfs, "Loaded at " + loadTime);

			return NanoHTTPD.newFixedLengthResponse(msg);
		} catch (Exception e) {
			e.printStackTrace();
			return NanoHTTPD.newFixedLengthResponse("Error loading page");
		}
		
	}

}
