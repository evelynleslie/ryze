package ryze.billing;

import java.util.Arrays;
import java.util.List;

import ryze.analytics.BuggyAnalysis;

public class BuggyBiller implements Biller<BuggyAnalysis, Double> {

	private double lowRate, highRate;

	public BuggyBiller() {
	};

	// lowRate and highRate are swapped
	public BuggyBiller(double loRate, double hiRate) {
		lowRate = loRate;
		highRate = hiRate;
	}

	@Override
	public List<Double> bill(List<? extends BuggyAnalysis> analysis) {
		BuggyAnalysis lysis = analysis.get(0);
		// getMaxLineLength() added lysis.getMinLineLength() instead of
		// subtracted by it
		// divided by 2..0 in order to perform double division
		double medianLineLength = (lysis.getMaxLineLength() + lysis.getMinLineLength()) / 2.0;
		System.out.println(medianLineLength);
		double applRate = lowRate;
		if (medianLineLength < lysis.getAvgLineLength()) {
			applRate = highRate;
		}
		return Arrays.asList(new Double[] { applRate * lysis.getTotalLines() });
	}

}
