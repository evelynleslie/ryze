package ryze.billing;

import java.util.LinkedList;
import java.util.List;

public class FlatRateBiller implements Biller<Number, Double> {

	private double rate = 0.01;
	
	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public FlatRateBiller(){}

	public FlatRateBiller(double rate){
		this();
		this.rate = rate;
	}



	@Override
	public List<Double> bill(List<? extends Number> analysis) {
		LinkedList<Double> res = new LinkedList<>();
		for(Number n: analysis ){
			res.add(rate * n.doubleValue());
		}
		return res;
	}

}
