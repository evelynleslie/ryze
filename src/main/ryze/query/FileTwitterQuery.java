package ryze.query;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import twitter4j.JSONException;
import twitter4j.JSONObject;
import twitter4j.JSONTokener;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterObjectFactory;


public class FileTwitterQuery implements Query<Tweet>{

	private static Logger LOG = Logger.getLogger(FileTwitterQuery.class.getName());
	
	public static final String DEFAULT_FILE = "data/twitter_data.json";
	
	private String fileName =  DEFAULT_FILE;
	
	private List<Tweet> tweets;
	
	public FileTwitterQuery(){
		loadFile();
	}
	
	private void loadFile(){
		
		tweets = new LinkedList<>();
		
		try (FileInputStream fis = new FileInputStream(fileName) ){
			JSONTokener json;
			try {
				json = new JSONTokener(fis);
				while(!json.end()){
					Object obj = json.nextValue();
					if(obj instanceof JSONObject){
						Status stat;
							try {
								stat = TwitterObjectFactory.createStatus(obj.toString());
								Tweet tweet = new Tweet(stat);
								tweets.add(tweet);
							} catch (TwitterException e) {
								// ignore exceptions thrown because not all JSON in the tweet file
								// can be converted into Status
							}
					}
				}
			} catch (JSONException e1) {
				//ignore JSON errors because the downloaded file is often incomplete and corrupted
			}
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} 
		
	}
	
	public void setFileName(String name){
		if(!fileName.equals(name)){
			fileName = name;
			loadFile();
		}
	}
	
	public String getFileName(){
		return fileName;
	}
	
	@Override
	public List<Tweet> query(String... keywords) {
		
		return tweets.stream()
			.filter(s -> contains(s, keywords))
			.collect(Collectors.toList());
	}

	private boolean contains(Tweet tweet, String... keywords){
		for(String word: keywords){
			Pattern ptn = Pattern.compile("\\b"+word+"\\b)", Pattern.CASE_INSENSITIVE);
			Matcher m = ptn.matcher(tweet.getText());
			if(m.find()) return true;
		}
		return false;
	}
}
