package ryze.query;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BuggyFileTextQuery extends SmallFileTextQuery {

	@Override
	boolean contains(String line, String... keywords){
		
		if(line.trim().startsWith("//")) return false;
		
		for(String word: keywords){
			Pattern ptrn = Pattern.compile(word);
			Matcher m = ptrn.matcher(line);
			if(m.find()) return true;
		}
		return false;
	}
}
