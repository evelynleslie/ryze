package ryze.workflow;

import ryze.analytics.Analyzer;
import ryze.billing.Biller;
import ryze.query.Query;

/**
 * An interface for workflows compposed of a Query, Analyzer and Biller.
 * 
 * @author whwong
 *
 * @param <A> Output type of Query for consumption by the Analyzer
 * @param <B> Output type of Analyzer for consumption by the Biller
 * @param <C> Output type of Biller
 */
public interface Workflow<A,B,C> {
	
	public Query<A> getQuery();
	public Analyzer<A,B> getAnalyzer();
	public Biller<B,C> getBiller();

}
